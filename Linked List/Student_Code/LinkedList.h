#pragma once
#include <stdexcept>
#include "LinkedListInterface.h"
#include <iostream>

using namespace std;

template<class T>
class LinkedList : public LinkedListInterface<T>
{

private:

	struct Node {
		T item;
		Node* next;
		Node* prev;
	};

	int count;
	Node* head;
	Node* tail;

public:

	~LinkedList()
	{
		this->clear();
	}
	void insertHead(T value)
	{
		if (find(value))
			return;
		Node* n = new Node();
		n->item = value;
		if (head == NULL)
		{
			tail = n;
			head = n;
			head->prev = NULL;
			head->next = tail;
			tail->prev = head;
			tail->next = NULL;
		}
		else
		{
			head->prev = n;

			n->next = head;
			n->prev = NULL;
			head = n;
		}
		count++;

	}
	void insertTail(T value)
	{
		if (find(value))
			return;
		Node* n = new Node();
		n->item = value;
		if (head == NULL)
		{
			tail = n;
			head = n;
			n->prev = NULL;
			n->next = NULL;
		}
		else
		{
			tail->next = n;

			n->prev = tail;
			n->next = NULL;
			tail = n;
		}
		count++;

	}
	void insertAfter(T value, T insertionNode)
	{
		if (find(value))
			return;
		Node* temp = find(insertionNode);
		if (temp == NULL)
			return;
		Node* n = new Node();
		n->item = value;
		if (temp != NULL)
		{
			if (temp->next != NULL)
			{
				n->next = temp->next;
				(n->next)->prev = n;
			}
			else
			{
				n->next = NULL;
				tail = n;
			}
			n->prev = temp;
			temp->next = n;
			count++;
		}
		
	}
	void remove(T value)
	{
		Node* n = find(value);
		if (n == NULL)
			return;
		if (n->item != head->item && n->item != tail->item)
		{
			if (n->prev != NULL && n->next != NULL)
			{
				(n->prev)->next = (n->next);
				(n->next)->prev = (n->prev);
			}
		}
		else if (count == 1)
		{
			head = NULL;
			tail = NULL;
		}
		else if (n->item == tail->item)
		{
			tail = n->prev;
			tail->next = NULL;
			
		}			
		else
		{
			head = n->next;
			head->prev = NULL;
			
		}

		delete n;
		count--;
	}
	void clear()
	{
		Node* temp = head;

		while (head != NULL) {
			temp = head;
			head = head->next;
			delete temp;
			
		}
		tail = head = NULL;
		count = 0;
	}
	T at(int index)
	{
		Node* n = head;
		int place = 0;
		if (index >= count || index < 0)
		{
			throw out_of_range("Out of Range");
		}
		while (n != NULL)
		{
			if (index == place)
				return n->item;
			else
				n = n->next;
			place++;
		}
		return head->item;

	}
	int size()
	{
		return count;
	}
	Node* find(T value)
	{
		Node* n = head;

		while (n != NULL) {
			if (value == n->item)
				return n;
			else
				n = n->next;
		}
		return NULL;
	}

};

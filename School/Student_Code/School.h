#pragma once
#include <sstream>
#include "GPAInterface.h"
#include "Student.h"
#include <iostream>
#include <fstream>


using namespace std;

class School : public GPAInterface
{
public:
	~School();
	map<unsigned long long int, StudentInterface*> getMap();
	set<StudentInterface*, Comparator> getSet();
	bool importStudents(string mapFileName, string setFileName);
	bool importGrades(string fileName);
	string querySet(string fileName);
	string queryMap(string fileName);
	void clear();

protected:
	map<unsigned long long int, StudentInterface*> students_m;
	set<StudentInterface*, Comparator> students_s;

};
#pragma once
#include <sstream>
#include <iomanip>
#include <iostream>
#include "StudentInterface.h"

using namespace std;

class Student : public StudentInterface
{

public:
	Student(unsigned long long int ID_in, string name_in, string addr_in, string phone_in);
	unsigned long long int getID();
	string getName();
	string getGPA();
	void addGPA(double classGrade);
	string toString();

protected:
	unsigned long long int ID;
	string addr, phone;
	string name;
	double GPA;
	double grade;
	int num_classes;

};
#include "Student.h"


Student::Student(unsigned long long int ID_in, string name_in, string addr_in, string phone_in)
{
	ID = ID_in;
	name = name_in;
	addr = addr_in;
	phone = phone_in;
	GPA = 0;
	grade = 0;
	num_classes = 0;
}


unsigned long long int Student::getID()
{
	return ID;
}
string Student::getName()
{
	return name;
}
string Student::getGPA()
{
	grade = (GPA / num_classes);
	stringstream temp;
	temp << fixed << setprecision(2) << grade;
	return temp.str();
}
void Student::addGPA(double classGrade)
{
	num_classes++;
	GPA += classGrade;
}
string Student::toString()
{
	string ID_s = to_string(ID);
	stringstream temp;
	if (num_classes == 0)
		grade = 0;
	else
		grade = (GPA / num_classes);
	temp << fixed << setprecision(2) << grade;
	string grade_s = temp.str();
	return ID_s + "\n" + name + "\n" + addr + "\n" + phone + "\n" + grade_s;

	
}
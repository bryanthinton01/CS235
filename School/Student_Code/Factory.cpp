#include "Factory.h"
#include "School.h"
#include "Student.h"
// You may put your include statements here
using namespace std;

/*
	You may modify this document
*/
//=======================================================================================
/*
	getGPA()

	Creates and returns an object whose class extends GPAInterface.
	This should be an object of a class you have created.

	Example: If you made a class called "GPA", you might say, "return new GPA();".
*/
GPAInterface * Factory::getGPA() {
    return new School;
}
//=======================================================================================

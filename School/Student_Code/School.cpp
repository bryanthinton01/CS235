#include "School.h"


unsigned long long int intCheck(string, bool &);

School::~School()
{
	this->clear();
}

map<unsigned long long int, StudentInterface*> School::getMap()
{
	return students_m;
}
set<StudentInterface*, Comparator> School::getSet()
{
	return students_s;
}
bool School::importStudents(string mapFileName, string setFileName)
{
	ifstream inputcheck(mapFileName);
	ifstream input2check(setFileName);
	ifstream input(mapFileName);
	if (input.fail())
		return false;
	ifstream input_s(setFileName);
	if (input.fail())
		return false;
	bool okay = true;
	string ID_s = "", name = "", addr = "", phone = "", temp;
	int test = 0;
	unsigned long long int ID;
	test = 0;
	while (getline(inputcheck, temp))
	{
		test++;
		
	}
	inputcheck.close();
	if (test % 4 != 0)
		return false;
	test = 0;
	while (getline(input2check, temp))
	{
		test++;
	}
	input2check.close();
	if (test % 4 != 0)
		return false;

	while (getline(input, ID_s))
	{
		ID = intCheck(ID_s, okay);
		if (okay == false)
			return false;
		getline(input, name);
		getline(input, addr);
		getline(input, phone);
		if (name == "" || addr == "" || phone == "")
			return false;
		else
			students_m.insert({ ID, new Student(ID, name, addr, phone) });
	}
	input.close();
	
	while (getline(input_s, ID_s))
	{
		ID = intCheck(ID_s, okay);
		if (okay == false)
			return false;
		getline(input_s, name);
		getline(input_s, addr);
		getline(input_s, phone);
	if (name == "" || addr == "" || phone == "")
		return false;
	else
		students_s.insert(new Student(ID, name, addr, phone));
	}
	input_s.close();

	return true;
}
bool School::importGrades(string fileName)
{
	ifstream input(fileName);
	if (input.fail())
		return false;
	string ID_s = "", course, grade;
	double grade_n;
	bool fail = true;
	unsigned long long int ID_n = 0;

	while (getline(input, ID_s))
	{
		ID_n = intCheck(ID_s, fail);
		getline(input, course);
		getline(input, grade);
				if (grade == "A")
					grade_n = 4.0;
				else if (grade == "A-")
					grade_n = 3.7;
				else if (grade == "B+")
					grade_n = 3.4;
				else if (grade == "B")
					grade_n = 3.0;
				else if (grade == "B-")
					grade_n = 2.7;
				else if (grade == "C+")
					grade_n = 2.4;
				else if (grade == "C")
					grade_n = 2.0;
				else if (grade == "C-")
					grade_n = 1.7;
				else if (grade == "D+")
					grade_n = 1.4;
				else if (grade == "D")
					grade_n = 1.0;
				else if (grade == "D-")
					grade_n = 0.7;
				else if (grade == "E")
					grade_n = 0.0;

				for (auto ID : students_s)
				{
					if (ID_n == ID->getID())
					{
						ID->addGPA(grade_n);
					}
				}
				for (map<unsigned long long int, StudentInterface*>::iterator it = students_m.begin(); it != students_m.end(); it++)
				{
					if (it->first == ID_n)
					{
						it->second->addGPA(grade_n);
					}
				}
		}
	
	return true;
}

string School::querySet(string fileName)
{
	ifstream input(fileName);
	string ID_s, output = "", temp;
	unsigned long long int ID_n = 0;
	bool fail;

	while (getline(input, ID_s))
	{
		ID_n = intCheck(ID_s, fail);
		for (auto ID : students_s)
		{
			if (ID_n == ID->getID())
			{
				temp = to_string(ID_n);
				output += temp + " " + ID->getGPA() + " " + ID->getName() + "\n";
			}
		}
	}
	return output;
	
}
string School::queryMap(string fileName)
{
	ifstream input(fileName);
	string ID_s, output = "", temp;
	unsigned long long int ID_n = 0;
	bool fail;

	while (getline(input, ID_s))
	{
		ID_n = intCheck(ID_s, fail);
		for (map<unsigned long long int, StudentInterface*>::iterator it = students_m.begin(); it != students_m.end(); it++)
		{
			if (ID_n == it->first)
			{
				temp = to_string(ID_n);
				output += temp + " " + it->second->getGPA() + " " + it->second->getName() + "\n";
			}
		}
	}
	return output;
}

void School::clear()
{
	for (auto ID : students_s)
	{
		delete ID;
		//students_s.erase(ID);
	}
	for (map<unsigned long long int, StudentInterface*>::iterator it = students_m.begin(); it != students_m.end(); it++)
	{
		delete it->second;
		//students_m.erase(it);
	}
	students_s.clear();
	students_m.clear();
}

unsigned long long int intCheck(string a, bool &fail)
{
	for (unsigned int i = 0; i < a.length(); i++)
	{
		if (a[i] != '0' && a[i] != '1' && a[i] != '2' && a[i] != '3' && a[i] != '4' && a[i] != '5' && a[i] != '6' && a[i] != '7' && a[i] != '8' && a[i] != '9')
		{
			fail = false;
			return -1;
		}
	}
	if (a == "")
	{
		fail = false;
		return -1;
	}
	double output = stoi(a.substr(0, a.length()).c_str());
	if (output == 0)
	{
		fail = false;
		return -1;
	}
	return output;
}
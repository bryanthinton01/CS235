//
#include "QS.h"

using namespace std;

QS::~QS()
{
	this->clear();
}

void QS::sortAll()
{
	int left = 0, right = size - 1;
	helper(left, right);
}
bool QS::helper(int left, int right)
{
	int pivot = medianOfThree(left, right);
	if ((right - left) < 3)
		return false;
	int x = partition(left, right, pivot);
	if (x == -1)
		return false;
	else
	{
		helper(left, x - 1);
		helper(x + 1, right);
	}
	return true;
}
int QS::medianOfThree(int left, int right)
{
	int temp;
	if (left < 0 || right >= size || size == 0 || right <= left)
		return -1;
	int middle = (left + right) / 2;
	if (array[left] > array[middle])
	{
		temp = array[left];
		array[left] = array[middle];
		array[middle] = temp;
	}
	if (array[right] < array[middle])
	{
		temp = array[right];
		array[right] = array[middle];
		array[middle] = temp;
	}
	if (array[left] > array[middle])
	{
		temp = array[left];
		array[left] = array[middle];
		array[middle] = temp;
	}
	return middle;
}
int QS::partition(int left, int right, int pivotIndex)
{

	if (array == NULL || left < 0 || right >= size || size == 0 || right <= left || pivotIndex <= left || pivotIndex >= right)
	{
		return -1;
	}
	int temp = array[pivotIndex];
	array[pivotIndex] = array[left];
	array[left] = temp;
	int i = left + 1;
	int j = right;
	while (j > i)
	{
		while (array[i] < array[left] && i < j)
			i++;
		while (array[j] > array[left] && j >= i)
			j--;
		if (j > i)
		{
			temp = array[i];
			array[i] = array[j];
			array[j] = temp;
		}
	}
	temp = array[left];
	array[left] = array[j];
	array[j] = temp;
	return j;
}
string QS::getArray()
{
	stringstream ss;
	for (int i = 0; i < size; i++)
	{
		if (i == size - 1)
			ss << array[i];
		else
			ss << array[i] << ",";
	}
	string out;
	ss >> out;
	return out;
}
int QS::getSize()
{
	return size;
}
void QS::addToArray(int value)
{
	if (size < cap)
	{
		array[size] = value;
		size++;
	}
}
bool QS::createArray(int capacity)
{
	cap = capacity;
	for (int i = 0; i < size; i++)
	{
		delete &array[i];
	}
	if (size > 0)
		delete[] array;
	if (capacity < 1)
		return false;
	else
	{
		array = new int[capacity];
		return true;
	}
}
void QS::clear()
{
	if (cap != 0)
		delete[] array;
	size = 0;
	cap = 0;
}



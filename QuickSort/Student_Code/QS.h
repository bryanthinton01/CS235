#pragma once
#include "QSInterface.h"
#include <sstream>

using namespace std;

class QS : public QSInterface
{
public:
	~QS();
	void sortAll();
	int medianOfThree(int left, int right);
	int partition(int left, int right, int pivotIndex);
	string getArray();
	int getSize();
	void addToArray(int value);
	bool createArray(int capacity);
	void clear();
	bool helper(int left, int right);

protected:
	int size = 0, cap;
	int* array;
};


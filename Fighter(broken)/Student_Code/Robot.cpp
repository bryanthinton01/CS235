#include "Robot.h"


Robot::Robot(string name_in, char type_in, int maxhp_in, int strength_in, int speed_in, int magic_in) : Fighter(name_in, type_in, maxhp_in, strength_in, speed_in, magic_in)
{
	current_energy = magic_in * 2;
	max_energy = current_energy;
}

int Robot::getDamage()
{
	int true_damage = strength + bonus_damage;
	return true_damage;
}

void Robot::reset()
{
	currenthp = maxhp;
	//Also resets a Robot's bonus damage (calculated when a Robot uses its ability) to 0.
}

void Robot::regenerate()
{
	int increase = ((1 / 6)*strength);
	if (increase < 1)
	{
		increase = 1;
	}
	currenthp += increase;
}

bool Robot::useAbility()
{
	if (current_energy >= ROBOT_ABILITY_COST)
	{
		bonus_damage = (strength  * ((current_energy / max_energy) ^ 4));
		current_energy -= ROBOT_ABILITY_COST;
		return true;
	}
	return false;
}
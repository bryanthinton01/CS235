#pragma once
#include <iostream>
#include <sstream>
#include <string>
#include "FighterInterface.h"
#include "Fighter.h"
//#include "Arena.h"


using namespace std;

class Cleric : public Fighter
{
public:
	Cleric(string name_in, char type_in, int maxhp_in, int strength_in, int speed_in, int magic_in);
	int getDamage();
	void reset();
	void regenerate();
	bool useAbility();
protected:
	int mana;
};
#include "Fighter.h"

using namespace std;

Fighter::Fighter(string name_in, char type_in, int maxhp_in, int strength_in, int speed_in, int magic_in)
{
	name = name_in;
	type = type_in;
	maxhp = maxhp_in;
	strength = strength_in;
	speed = speed_in;
	magic = magic_in;
}
string Fighter::getName()
{
	return name;
}
int Fighter::getMaximumHP()
{
	return maxhp;
}
int Fighter::getCurrentHP()
{
	return currenthp;
}
int Fighter::getStrength()
{
	return strength;
}
int Fighter::getSpeed()
{
	return speed;
}
int Fighter::getMagic()
{
	return magic;
}
void Fighter::takeDamage(int damage)
{
	int reduction = (damage - (.25 * speed));
	if (reduction < 1)
	{
		reduction = 1;
	}
	currenthp -= reduction;
}

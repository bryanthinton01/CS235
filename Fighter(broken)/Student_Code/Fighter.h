#pragma once
#include <iostream>
#include <sstream>
#include <string>
#include "FighterInterface.h"


using namespace std;

class Fighter : public FighterInterface
{
public:
	Fighter(string name_in, char type_in, int maxhp_in, int strength_in, int speed_in, int magic_in);
	string getName();
	int getMaximumHP();
	int getCurrentHP();
	int getStrength();
	int getSpeed();
	int getMagic();
	void takeDamage(int damage);
	virtual bool useAbility() = 0;
	virtual void regenerate() = 0;
	virtual void reset() = 0;
	virtual int getDamage() = 0;
protected:
	string name;
	char type;
	int maxhp, strength, speed, magic;
	int currenthp, damage;
};
#include "Arena.h"

using namespace std;

int intCheck(string, int &);
char charCheck(string, int &);

bool Arena::addFighter(string info)
{
	int fail = 0;
	string type_str, maxhp_str, strength_str, speed_str, magic_str;
	cin >> name;
	
	for (unsigned int i = 0; i < fighters.size(); i++)
	{
		if (fighters[i]->getName() == name)
		{
			fail++;
		}
	}

	cin >> type_str;
	type = charCheck(type_str, fail);
	if (type == 'A' || type == 'C' || type == 'R') {
	}
	else {
		fail++;
	}
		
	cin >> maxhp_str >> strength_str >> speed_str >> magic_str;
	maxhp = intCheck(maxhp_str, fail);
	strength = intCheck(strength_str, fail);
	speed = intCheck(speed_str, fail);
	magic = intCheck(magic_str, fail);
		
	if (fail == 0)
	{
		if (type == 'A')
		fighters.push_back(new Archer(name, type, maxhp, strength, speed, magic));
		else if (type == 'C')
		fighters.push_back(new Cleric(name, type, maxhp, strength, speed, magic));
		else if (type == 'R')
		fighters.push_back(new Robot(name, type, maxhp, strength, speed, magic));
		return true;
	}
	else
	{
		fail = 0;
		return false;
	}


}
bool Arena::removeFighter(string name)
{
	
	for (unsigned int i = 0; i < fighters.size(); i++)
	{
		if (fighters[i]->getName() == name)
		{
			fighters.erase(fighters.begin() + i);
			return true;
		}
	}
	return false;
}

FighterInterface* Arena::getFighter(string name)
{
	for (unsigned int i = 0; i < fighters.size(); i++)
	{
		if (fighters[i]->getName() == name)
		{
			return fighters[i];
		}
	}
	return 0;
}

int Arena::getSize()
{
	return fighters.size();
}

int intCheck(string a, int &fail)
{
	for (unsigned int i = 0; i < a.length(); i++)
	{
		if (a[i] != '0' && a[i] != '1' && a[i] != '2' && a[i] != '3' && a[i] != '4' && a[i] != '5' && a[i] != '6' && a[i] != '7' && a[i] != '8' && a[i] != '9')
		{
			fail++;
			return -1;
		}
	}
	int output = stoi(a.substr(0, a.length()).c_str());
	
	return output;
}
char charCheck(string a, int &fail)
{
	char check;
	if (a.size() == 1)
	{
		check = a[0];
		return check;
	}
	else
	{
		fail++;
		return 'Q';
	}
}

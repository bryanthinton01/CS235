#include "Archer.h"

Archer::Archer(string name_in, char type_in, int maxhp_in, int strength_in, int speed_in, int magic_in) : Fighter(name_in, type_in, maxhp_in, strength_in, speed_in, magic_in)
{
	maxspeed = speed_in;
}

int Archer::getDamage()
{
	return speed;
}

void Archer::reset()
{
	currenthp = maxhp;
	speed = maxspeed;
}


void Archer::regenerate()
{
	int increase = ((1 / 6)*strength);
	if (increase < 1)
	{
		increase = 1;
	}
	currenthp += increase;
}

bool Archer::useAbility()
{
	speed += 1;
	return true;
}
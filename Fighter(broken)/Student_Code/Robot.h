#pragma once
#include <iostream>
#include <sstream>
#include <string>
#include "Fighter.h"



using namespace std;

class Robot : public Fighter
{
public:
	Robot(string name_in, char type_in, int maxhp_in, int strength_in, int speed_in, int magic_in);
	int getDamage();
	void reset();
	void regenerate();
	bool useAbility();
protected:
	int current_energy, max_energy, bonus_damage;
};
#pragma once
#include <iostream>
#include <sstream>
#include <string>
#include "ArenaInterface.h"
#include "Fighter.h"
#include "Archer.h"
#include "Robot.h"
#include "Cleric.h"

using namespace std;

class Arena : public ArenaInterface
{
public:
	bool addFighter(string info);
	bool removeFighter(string name);
	FighterInterface* getFighter(string name);
	int getSize();

protected:
	vector<FighterInterface*> fighters;
	string name;
	char type;
	int maxhp, strength, speed, magic;

};
#include "Cleric.h"
using namespace std;


Cleric::Cleric(string name_in, char type_in, int maxhp_in, int strength_in, int speed_in, int magic_in) : Fighter(name_in, type_in, maxhp_in, strength_in, speed_in, magic_in)
{
	mana = magic_in * 5;
}

int Cleric::getDamage()
{
	return magic;
}

void Cleric::reset()
{
	currenthp = maxhp;
	mana = magic * 5;
}

void Cleric::regenerate()
{
	int increase = ((1 / 6)*strength);
	if (increase < 1)
	{
		increase = 1;
	}
	currenthp += increase;
	int increase2 = ((1 / 5)*magic);
	if (increase2 < 1)
	{
		increase2 = 1;
	}
	mana += increase2;
	if (mana > (magic * 5))
	{
		mana = (magic * 5);
	}
}

bool Cleric::useAbility()
{
	int increase = ((1 / 3)*magic);
	if (mana >= CLERIC_ABILITY_COST)
	{
		if (increase < 1)
		{
			increase = 1;
		}
		currenthp += ((1 / 3)*magic);
		if (currenthp > maxhp)
		{
			currenthp = maxhp;
		}
		mana -= CLERIC_ABILITY_COST;
		return true;
	}
	return false;
}

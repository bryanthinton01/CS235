#include "AVL.h"
#include <iostream>


AVL::~AVL()
{
	this->clear();
}
Node* AVL::getRootNode()
{
	return root;
}
bool AVL::add(int data)
{
	return addHelp(root, data);
}
bool AVL::remove(int data)
{
	return remHelp(root, data);
}
void AVL::clear()
{
	clearHelp(root);
	return;
}
bool AVL::addHelp(Node*& n, int data)
{
	if (n == NULL)
	{
		n = new Node();
		n->data = data;
		if (!root)
		{
			root = n;
		}
		increase = true;
		return true;
	}
	else if (data > n->data)
	{
		bool returnValue = addHelp(n->right, data);
		if (increase)
		{
			switch (n->balance)
			{
			case Node::BALANCED:
				n->balance = Node::RIGHT_HEAVY;
				break;
			case Node::LEFT_HEAVY:
				n->balance = Node::BALANCED;
				increase = false;
				break;
			case Node::RIGHT_HEAVY:
				balToRight(n);
				increase = false;
				break;
			}
		}
		return returnValue;
	}
	else if (data < n->data)
	{
		bool returnValue = addHelp(n->left, data);
		if (increase)
		{
			switch (n->balance)
			{
				case Node::BALANCED :
					n->balance = Node::LEFT_HEAVY;
					break;
				case Node::RIGHT_HEAVY :
					n->balance = Node::BALANCED;
					increase = false;
					break;
				case Node::LEFT_HEAVY :
					balToLeft(n);
					increase = false;
					break;
			}
		}
		return returnValue;
	}
	else
	{
		increase = false;
		return false;
	}
}
bool AVL::remHelp(Node*& n, int data)
{
	Node* q = NULL;
	if (n == NULL)
		return false; 
	
	else if (data > n->data)
	{
		bool returnValue = remHelp(n->right, data);
		if (decrease)
		{
			switch (n->balance)
			{
			case Node::BALANCED:
				n->balance = Node::LEFT_HEAVY;
				decrease = false;
				break;
			case Node::RIGHT_HEAVY:
				n->balance = Node::BALANCED;
				decrease = true;
				break;
			case Node::LEFT_HEAVY:
			if (n->data > n->left->data && n->right == NULL && n->left->right == NULL && n->left->left == NULL);
			else
				balToLeft(n);
			decrease = false;
			break;
			}
		}
		if (test(n->left) - test(n->right) > 1 && test2(n->left) - test2(n->right) > 1)
			balToLeft(n);
		return returnValue;
	}
	else if (data < n->data)
	{
		bool returnValue = remHelp(n->left, data);
		if (decrease)
		{
			switch (n->balance)
			{
			case Node::BALANCED:
				n->balance = Node::RIGHT_HEAVY;
				decrease = false;
				break;
			case Node::LEFT_HEAVY:
				n->balance = Node::BALANCED;
				decrease = true;
				break;
			case Node::RIGHT_HEAVY:
				if (n->data < n->right->data && n->left == NULL && n->right->left == NULL && n->right->right == NULL);
				else
					balToRight(n);
				decrease = false;
				break;
			}
		}
		if (test(n->right) - test(n->left) > 1 && test2(n->right) - test2(n->left) > 1)
			balToRight(n);
		return returnValue;
	}
	else
	{
		if (n->left == NULL && n->right == NULL)
		{
			if (n == root)
			{
				delete root;
				root = NULL;
			}
			else
			{
				delete n;
				n = NULL;
				decrease = true;
			}
			return true;
		}
		else if (n->left == NULL)
		{
			q = n;
			n = n->right;
			delete q;
			q = NULL;
			decrease = true;
			return true;
		}
		else if (n->right == NULL)
		{
			q = n;
			n = n->left;
			delete q;
			q = NULL;
			decrease = true;
			return true;
		}
		else
		{
			decrease = false;
			n->data = findMax(n->left);
			remHelp(n->left, n->data);
			return true;
		}
		return true;
	}
}
int AVL::findMax(Node*& n)
{
	if (n->right == NULL)
		return n->data;
	else
	{
		return findMax(n->right);
	}
}
void AVL::clearHelp(Node*& leaf)
{
	if (leaf != NULL)
	{
		clearHelp(leaf->left);
		clearHelp(leaf->right);
		delete leaf;
		leaf = NULL;
		return;
	}

	return;
}
void AVL::balToRight(Node*& n)
{
	Node* r = n->right;
	if (r->balance == Node::LEFT_HEAVY)
	{
		if (r->left->balance == Node::RIGHT_HEAVY)
		{
			decrease = true;
			r->balance = Node::BALANCED;
			r->left->balance = Node::BALANCED;
			n->balance = Node::LEFT_HEAVY;
			//cout << "20" << endl;
		}
		else if (r->left->balance == Node::BALANCED)
		{
			decrease = false;
			r->balance = Node::BALANCED;
			r->left->balance = Node::BALANCED;
			n->balance = Node::BALANCED;
			//cout << "21" << endl;
		}
		else
		{
			decrease = true;
			r->balance = Node::RIGHT_HEAVY;
			r->left->balance = Node::BALANCED;
			n->balance = Node::BALANCED;
			//cout << "22" << endl;
		}
		rotRight(r);
	}
	else
	{
		decrease = false;
		r->balance = Node::BALANCED;
		n->balance = Node::BALANCED;
		//cout << "23" << endl;
	}
	rotLeft(n);
}
void AVL::balToLeft(Node*& n)
{
	Node* l = n->left;
	if (l->balance == Node::RIGHT_HEAVY)
	{
		if (l->right->balance == Node::LEFT_HEAVY)
		{
			decrease = true;
			l->balance = Node::BALANCED;
			l->right->balance = Node::BALANCED;
			n->balance = Node::RIGHT_HEAVY;
			//cout << "24" << endl;
		}
		else if (l->right->balance == Node::BALANCED)
		{
			decrease = false;
			l->balance = Node::BALANCED;
			l->right->balance = Node::BALANCED;
			n->balance = Node::BALANCED;
			//cout << "25" << endl;
		}
		else
		{
			decrease = true;
			l->balance = Node::LEFT_HEAVY;
			l->right->balance = Node::BALANCED;
			n->balance = Node::BALANCED;
			//cout << "26" << endl;
		}
		rotLeft(l);
	}
	else
	{
		decrease = false;
		l->balance = Node::BALANCED;
		n->balance = Node::BALANCED;
		//cout << "27" << endl;
	}
	rotRight(n);
}
void AVL::rotRight(Node*& n)
{
	Node* temp = n->left;
	n->left = temp->right;
	temp->right = n;
	n = temp;
}
void AVL::rotLeft(Node*& n)
{
	Node* temp = n->right;
	n->right = temp->left;
	temp->left = n;
	n = temp;
}
int AVL::test(Node*& n)
{
	if (n != NULL)
	{
		if (n->right != NULL)
			return 1 + test(n->right);
		else
			return 1 + test(n->left);
	}
	return 0;
}
int AVL::test2(Node*& n)
{
	if (n != NULL)
	{
		if (n->left != NULL)
			return 1 + test2(n->left);
		else
			return 1 + test2(n->right);
	}
	return 0;
}
#pragma once

#include <iostream>
#include "NodeInterface.h"

class Node : public NodeInterface {

public:

	enum balance_type { LEFT_HEAVY = -1, BALANCED = 0, RIGHT_HEAVY = +1 };
	balance_type balance;
	Node(Node* left = NULL, Node* right = NULL) : balance(BALANCED) {}

	int getData();
	Node* getLeftChild();
	Node* getRightChild();
	int getHeight();
	friend class AVL;

private:
	Node* left = NULL;
	Node* right = NULL;
	int data;
	int height;

};
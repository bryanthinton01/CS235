#pragma once

#include "Node.h"
#include "BSTInterface.h"

using namespace std;

class BST : public BSTInterface {
public:
	~BST();
	Node* getRootNode();
	bool add(int data);
	bool remove(int data);
	void clear();
	bool find(Node*&, int data);
	bool addHelp(Node*&, int data);
	bool remHelp(Node*&, int data);
	int findMax(Node*&);
	void clearHelp(Node*&);

private:
	Node* root = NULL;

};
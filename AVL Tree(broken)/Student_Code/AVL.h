#pragma once

#include "Node.h"
#include "AVLInterface.h"

using namespace std;

class AVL : public AVLInterface {
public:
	~AVL();
	Node* getRootNode();
	bool add(int data);
	bool remove(int data);
	void clear();
	bool addHelp(Node*&, int data);
	bool remHelp(Node*&, int data);
	int findMax(Node*&);
	void clearHelp(Node*&);
	void balToRight(Node*&);
	void balToLeft(Node*&);
	void rotRight(Node*&);
	void rotLeft(Node*&);
	int test(Node*&);
	int test2(Node*&);

private:
	Node* root = NULL;
	bool increase;
	bool decrease;
};
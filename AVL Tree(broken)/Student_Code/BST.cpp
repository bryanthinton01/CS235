#include "BST.h"

BST::~BST()
{
	this->clear();
}
Node* BST::getRootNode()
{
	return root;
}
bool BST::add(int data)
{
	return addHelp(root, data);
}
bool BST::remove(int data)
{
	return remHelp(root, data);
}
void BST::clear()
{
	clearHelp(root);
	return;
}
bool BST::find(Node*& n, int data)
{
	Node* q = NULL;
	if (n == NULL)
		return false;
	else if (data > n->data)
	{
		q = n->right;
		find(q, data);
		return true;
	}
	else if (data < n->data)
	{
		q = n->left;
		find(q, data);
		return true;
	}
	else
		return true;
}
bool BST::addHelp(Node*& n, int data)
{
	Node* q = NULL;
	if (root == NULL)
	{
		n = new Node();
		n->data = data;
		root = n;
		return true;
	}
	else if (n->left == NULL && n->data > data)
	{
		q = new Node();
		n->left = q;
		q->data = data;
		return true;
	}
	else if (n->right == NULL && n->data < data)
	{
		q = new Node();
		n->right = q;
		q->data = data;
		return true;
	}
	else if (data > n->data)
	{
		q = n->right;
		addHelp(q, data);
		return true;
	}
	else if (data < n->data)
	{
		q = n->left;
		addHelp(q, data);
		return true;
	}
	else
		return false;
}
bool BST::remHelp(Node*& n, int data)
{
		Node* q = NULL;
		if (n == NULL)
			return false;
		else if (data > n->data)
		{
			remHelp(n->right, data);
			return true;
		}
		else if (data < n->data)
		{
			remHelp(n->left, data);
			return true;
		}
		else
		{
			if (n->left == NULL && n->right == NULL)
			{
				if (n == root)
				{
					delete root;
					root = NULL;
				}
				else
				{
					delete n;
					n = NULL;
				}
				return true;
			}
			else if (n->left == NULL)
			{
				q = n;
				n = n->right;
				delete q;
				q = NULL;
				return true;
			}
			else if (n->right == NULL)
			{
				q = n;
				n = n->left;
				delete q;
				q = NULL;
				return true;
			}
			else
			{
				n->data = findMax(n->left);
				remHelp(n->left, n->data);
				return true;
		}
			return true;

	}
}
int BST::findMax(Node*& n)
{
	if (n->right == NULL)
		return n->data;
	else
	{
		return findMax(n->right);
	}
}
void BST::clearHelp(Node*& leaf)
{
	if (leaf != NULL)
	{
		clearHelp(leaf->left);
		clearHelp(leaf->right);
		delete leaf;
		leaf = NULL;
		return;
	}
	
	return;
}
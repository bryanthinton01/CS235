#include "ExpressionManager.h"
#include <sstream>

bool isLeftParen(string);
bool isRightPeren(string);
bool isPair(string, string);
bool isOperator(string);
bool isNumber(string);
int precedence(string);
string operate(int, int, string);


ExpressionManager::ExpressionManager()
{

}

bool ExpressionManager::isBalanced(string expression)
{
	string bracket;
	stack<string> s;
	stringstream in(expression);

	while (in >> bracket)
	{
		if (isLeftParen(bracket) == true)
			s.push(bracket);
		if (isRightPeren(bracket) == true)
		{
			if (s.empty() == true)
				return false;
			if (isPair(s.top(), bracket) == true)
				s.pop();
			else
				return false;
		}
	}
	if (s.empty() == true)
		return true;
	else
		return false;

}
string ExpressionManager::postfixToInfix(string postfixExpression)
{
	string temp, temp_s1, temp_s2, final_s;
	stack<string> s;
	stringstream in(postfixExpression);
	stringstream conv;

	while (in >> temp)
	{
		if (isNumber(temp) == false && isOperator(temp) == false)
			return "invalid";
		if (isNumber(temp) == true)
			s.push(temp);
		if (isOperator(temp) == true)
		{
			if (s.size() < 2)
				return "invalid";
			else
			{
				temp_s1 = s.top();
				s.pop();
				temp_s2 = s.top();
				s.pop();
				final_s = "( " + temp_s2 + " " + temp + " " + temp_s1 + " )";
				s.push(final_s);
			}
		}
	}
	if (s.size() == 1)
		return s.top();
	else
		return "invalid";
}
string ExpressionManager::infixToPostfix(string infixExpression)
{
	string temp, output;
	int last;
	stack<string> s;
	stringstream in(infixExpression);

	while (in >> temp)
	{
		if (isNumber(temp) == true || isOperator(temp) == true || isRightPeren(temp) == true || isLeftParen(temp) == true)
		{
			if (isNumber(temp) == true)
			{
				output += (" " + temp);
				last = 0;
			}
			else if (precedence(temp) == 0)
			{
				s.push(temp);
				last = 0;
			}
			else if (precedence(temp) == 1)
			{
				if (last == 1 || last == 2)
					return "invalid";
				last = precedence(temp);
				if (s.empty() == true || precedence(s.top()) == 0)
					s.push(temp);
				else
				{
					while (precedence(s.top()) > 0)
					{
						output += (" " + s.top());
						s.pop();
						if (s.empty())
							break;
					}
					s.push(temp);
				}
			}
			else if (precedence(temp) == 2)
			{
				if (last == 2 || last == 1)
					return "invalid";
				last = precedence(temp);
				if (s.empty() == true || precedence(s.top()) == 0 || precedence(s.top()) == 1)
					s.push(temp);
				else
				{
					while (precedence(s.top()) > 1)
					{

						output += (" " + s.top());
						s.pop();
						if (s.empty())
							break;
					}
					s.push(temp);
				}
			}
			else if (precedence(temp) == 3)
			{
				if (last == 2 || last == 1)
					return "invalid";
				last = precedence(temp);
				while (isPair(s.top(), temp) == false)
				{
					output += (" " + s.top());
					s.pop();
					if (s.empty())
						return "invalid";
				}
				s.pop();
			}
		}
		else
			return "invalid";
	}
	if (output == "" || isLeftParen(s.top()) == true)
		return "invalid";
	while (!s.empty())
	{
		output += (" " + s.top());
		s.pop();
	}

	return output;
}
string ExpressionManager::postfixEvaluate(string postfixExpression)
{
	string temp, temp_s2;
	int temp_n1, temp_n2;
	stack<string> s;
	stringstream in(postfixExpression);
	stringstream conv;
	
	while (in >> temp)
	{
		if (isNumber(temp) == false && isOperator(temp) == false)
			return "invalid";
		if (isNumber(temp) == true)
			s.push(temp);
		if (isOperator(temp) == true)
		{
			if (s.size() < 2)
				return "invalid";
			else
			{
				conv << s.top();
				conv >> temp_n1;
				conv.str("");
				conv.clear();
				s.pop();
				conv << s.top();
				conv >> temp_n2;
				conv.str("");
				conv.clear();
				s.pop();
				temp_s2 = operate(temp_n2, temp_n1, temp);
				if (temp_s2 == "invalid")
					return temp_s2;
				else
					s.push(temp_s2);
			}
		}
	}
	return s.top();
}

bool isLeftParen(string t)
{
	if (t == "(" || t == "[" || t == "{")
		return true;
	else
		return false;
}
bool isRightPeren(string t)
{
	if (t == ")" || t == "]" || t == "}")
		return true;
	else
		return false;
}
bool isPair(string left, string right)
{
	if (left == "(" && right == ")")
		return true;
	if (left == "[" && right == "]")
		return true;
	if (left == "{" && right == "}")
		return true;
	else
		return false;
}
bool isOperator(string t)
{
	if (t == "+" || t == "*" || t == "-" || t == "/" || t == "%")
		return true;
	else
		return false;
}
bool isNumber(string t)
{

	for (auto c : t)
	{
		if (c != '0' && c != '1' && c != '2' && c != '3' && c != '4' && c != '5' && c != '6' && c != '7' && c != '8' && c != '9')
			return false;
	}
	return true;
}
int precedence(string t)
{
	if (t == ")" || t == "]" || t == "}")
		return 3;
	else if (t == "*" || t == "/" || t == "%")
		return 2;
	else if (t == "+" || t == "-")
		return 1;
	else if (t == "(" || t == "{" || t == "[")
		return 0;
	else 
		return 4;
}
string operate(int a, int b, string op)
{
	stringstream conv;
	string temp;
	if (op == "+")
	{
		conv << (a + b);
		conv >> temp;
		return temp;
	}
	if (op == "-")
	{
		conv << (a - b);
		conv >> temp;
		return temp;
	}
	if (op == "*")
	{
		conv << (a * b);
		conv >> temp;
		return temp;
	}
	if (op == "/")
	{
		if (b == 0)
			return "invalid";
		conv << (a / b);
		conv >> temp;
		return temp;
	}
	if (op == "%")
	{
		conv << (a % b);
		conv >> temp;
		return temp;
	}
	return "invalid";
}
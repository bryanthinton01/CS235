#pragma once
#include <cstdlib>
#include <sstream>
#include <algorithm>
#include <string>
#include <array>
#include <fstream>
#include "PathfinderInterface.h"

using namespace std;

class Pathfinder : public PathfinderInterface
{
public:
	Pathfinder();
	string getMaze();
	void createRandomMaze();
	bool importMaze(string file_name);
	vector<string> solveMaze();
	bool solve(int x, int y, int z);

protected:
	int maze[5][5][5];
	vector<string> empty;
	vector<string> path;
};

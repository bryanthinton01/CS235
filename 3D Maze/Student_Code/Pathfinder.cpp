#include "Pathfinder.h"

using namespace std;

Pathfinder::Pathfinder()
{
	int i = 0, j = 0, k = 0;
	while (k < 5)
	{
		if (i < 5)
		{
			maze[i][j][k] = 1;
			i++;
		}
		else
		{
			i = 0;
			j++;
		}

		if (j == 5)
		{
			i = 0;
			j = 0;
			k++;
		}
	}
}

string Pathfinder::getMaze()
{
	ostringstream in;
	string out;
	int i = 0, j = 0, k = 0;
	while(k < 5)
	{
		if (i < 5)
		{
			in << maze[i][j][k];
			in << " ";
			i++;
		}
		else
		{
			in << "\n";
			i = 0;
			j++;
		}
		
		if (j == 5)
		{
			in << "\n";
			i = 0;
			j = 0;
			k++;
		}	
	}
	out = in.str();
	return out;
}

void Pathfinder::createRandomMaze()
{
	string out;
	int i = 0, j = 0, k = 0;
	while (k < 5)
	{
		if (i < 5)
		{
			maze[i][j][k] = rand() % 2;
			i++;
		}
		else
		{
			i = 0;
			j++;
		}

		if (j == 5)
		{
			i = 0;
			j = 0;
			k++;
		}
	}
	maze[0][0][0] = 1;
	maze[4][4][4] = 1;
}

bool Pathfinder::importMaze(string file_name)
{
	int tempMaze[5][5][5];
	int i = 0, j = 0, k = 0;
	ifstream in(file_name);
	if (in.fail())
	{
		return false;
	}
	while (k < 5)
	{
		if (i < 5)
		{
			in >> tempMaze[i][j][k];
			if (tempMaze[i][j][k] != 0 && tempMaze[i][j][k] != 1)
				return false;
			i++;
		}
		else
		{
			i = 0;
			j++;
		}

		if (j == 5)
		{
			i = 0;
			j = 0;
			k++;
		}
	}
	i = 0;
	j = 0;
	k = 0;
	if (tempMaze[0][0][0] == 0 || tempMaze[4][4][4] == 0)
		return false;
	else if (in >> tempMaze[i][j][k])
		return false;
	while (k < 5)
	{
		if (i < 5)
		{
			maze[i][j][k] = tempMaze[i][j][k];
			i++;
		}
		else
		{
			i = 0;
			j++;
		}

		if (j == 5)
		{
			i = 0;
			j = 0;
			k++;
		}
	}
	return true;
}

vector<string> Pathfinder::solveMaze()
{
	path.clear();
	if (solve(0, 0, 0) == true)
	{
		int i = 0, j = 0, k = 0;
		while (k < 5)
		{
			if (i < 5)
			{
				if (maze[i][j][k] == 2)
				{
					maze[i][j][k] = 1;
				}
				i++;
			}
			else
			{
				i = 0;
				j++;
			}

			if (j == 5)
			{
				i = 0;
				j = 0;
				k++;
			}
		}
		for (auto s : path) // Lines 180 through 189 are needed for some reason and I don't know why!!
		{
			i++;
			if (s == "(4, 4, 4)")
			{
				path.erase(path.begin()+i, path.end());
			}
		}
		if (path.back() != "(4, 4, 4)")
			return empty;
		return path;
	}
	else
		return empty;
}

bool Pathfinder::solve(int x, int y, int z)
{
	stringstream ss, ss1, ss2;
	if (x > 4 || x < 0 || y > 4 || y < 0 || z > 4 || z < 0)
		return false;
	else if (maze[x][y][z] != 1)
		return false;
	else
	{
		ss << x;
		string x_string;
		ss >> x_string;
		ss1 << y;
		string y_string;
		ss1 >> y_string;
		ss2 << z;
		string z_string;
		ss2 >> z_string;
		path.push_back("(" + x_string + ", " + y_string + ", " + z_string + ")");
		maze[x][y][z] = 2;
		solve(x, y, z + 1);
		solve(x, y, z - 1);
		solve(x, y + 1, z);
		solve(x, y - 1, z);
		solve(x + 1, y, z);
		solve(x - 1, y, z);
	}
	if (path.back() == "(4, 4, 4)")
		return true;
	return true;
}